# RuoYi +Echarts+MySql 存储过程 实现图表可视化
## 注意事项:
> 1. 注意项目使用访问端口 82;
> 2. 数据库账号密码与端口;
> 3. JDK 默认JDK 11
> 
> _使用时请自行修改对应信息_ 
## 系统简述
> 1. 项目主体使用`ruoyi-fast` 版进行设计,若依系统使用请参考若依官网Api;
> 2. 系统中使用了有参存储过程、无参存储过程；
> 3. 使用`echarts.js` 对返回数据进行简单可视化处理；
> - 效果图:
    ![](src/main/resources/static/img/Snipaste_2021-04-13_17-55-27.png)
> 
## 更多信息
> _请查看微信公众号：_
> - 公众号：V5codings
> 
>      ![](src/main/resources/static/img/Snipaste_2021-04-13_18-18-31-v5.png)
> - 公众号：若依框架教程
>    
>    ![](src/main/resources/static/img/Snipaste_2021-04-13_18-17-01-ry.png)