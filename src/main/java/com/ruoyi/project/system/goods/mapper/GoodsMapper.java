package com.ruoyi.project.system.goods.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.project.system.goods.domain.Goods;

/**
 * goodsMapper接口
 * 
 * @author HRuinger
 * @date 2021-04-12
 */
public interface GoodsMapper 
{
    /**
     * 查询goods
     * 
     * @param gid goodsID
     * @return goods
     */
    public Goods selectGoodsById(Long gid);

    /**
     * 查询goods列表
     * 
     * @param goods goods
     * @return goods集合
     */
    public List<Goods> selectGoodsList(Goods goods);

    /**
     * 新增goods
     * 
     * @param goods goods
     * @return 结果
     */
    public int insertGoods(Goods goods);

    /**
     * 修改goods
     * 
     * @param goods goods
     * @return 结果
     */
    public int updateGoods(Goods goods);

    /**
     * 删除goods
     * 
     * @param gid goodsID
     * @return 结果
     */
    public int deleteGoodsById(Long gid);

    /**
     * 批量删除goods
     * 
     * @param gids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsByIds(String[] gids);
    public List<Goods> getData ();
    void getCtn(Map map);

}
