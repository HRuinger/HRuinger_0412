package com.ruoyi.project.system.goods.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.goods.domain.Goods;
import com.ruoyi.project.system.goods.service.IGoodsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * goodsController
 * 
 * @author HRuinger
 * @date 2021-04-12
 */
@Controller
@RequestMapping("/system/goods")
public class GoodsController extends BaseController
{
    private String prefix = "system/goods";

    @Autowired
    private IGoodsService goodsService;

    @RequiresPermissions("system:goods:view")
    @GetMapping()
    public String goods()
    {
        return prefix + "/goods";
    }

    @GetMapping("/getData")
    @ResponseBody
    public List< Goods> getData ( ) {
        return goodsService.getData ( );
    }

    @GetMapping("/getCtn")
    @ResponseBody
    public Double getCtn ( ) {
        return goodsService.getCtn ();
    }

    /**
     * 查询goods列表
     */
    @RequiresPermissions("system:goods:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Goods goods)
    {
        startPage();
        List<Goods> list = goodsService.selectGoodsList(goods);
        return getDataTable(list);
    }

    /**
     * 导出goods列表
     */
    @RequiresPermissions("system:goods:export")
    @Log(title = "goods", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Goods goods)
    {
        List<Goods> list = goodsService.selectGoodsList(goods);
        ExcelUtil<Goods> util = new ExcelUtil<Goods>(Goods.class);
        return util.exportExcel(list, "goods");
    }

    /**
     * 新增goods
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存goods
     */
    @RequiresPermissions("system:goods:add")
    @Log(title = "goods", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Goods goods)
    {
        return toAjax(goodsService.insertGoods(goods));
    }

    /**
     * 修改goods
     */
    @GetMapping("/edit/{gid}")
    public String edit(@PathVariable("gid") Long gid, ModelMap mmap)
    {
        Goods goods = goodsService.selectGoodsById(gid);
        mmap.put("goods", goods);
        return prefix + "/edit";
    }

    /**
     * 修改保存goods
     */
    @RequiresPermissions("system:goods:edit")
    @Log(title = "goods", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Goods goods)
    {
        return toAjax(goodsService.updateGoods(goods));
    }

    /**
     * 删除goods
     */
    @RequiresPermissions("system:goods:remove")
    @Log(title = "goods", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(goodsService.deleteGoodsByIds(ids));
    }
}
