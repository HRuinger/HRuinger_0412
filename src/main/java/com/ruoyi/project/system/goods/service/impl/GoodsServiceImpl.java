package com.ruoyi.project.system.goods.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.goods.mapper.GoodsMapper;
import com.ruoyi.project.system.goods.domain.Goods;
import com.ruoyi.project.system.goods.service.IGoodsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * goodsService业务层处理
 * 
 * @author HRuinger
 * @date 2021-04-12
 */
@Service
public class GoodsServiceImpl implements IGoodsService 
{
    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 查询goods
     * 
     * @param gid goodsID
     * @return goods
     */
    @Override
    public Goods selectGoodsById(Long gid)
    {
        return goodsMapper.selectGoodsById(gid);
    }

    /**
     * 查询goods列表
     * 
     * @param goods goods
     * @return goods
     */
    @Override
    public List<Goods> selectGoodsList(Goods goods)
    {
        return goodsMapper.selectGoodsList(goods);
    }

    /**
     * 新增goods
     * 
     * @param goods goods
     * @return 结果
     */
    @Override
    public int insertGoods(Goods goods)
    {
        return goodsMapper.insertGoods(goods);
    }

    /**
     * 修改goods
     * 
     * @param goods goods
     * @return 结果
     */
    @Override
    public int updateGoods(Goods goods)
    {
        return goodsMapper.updateGoods(goods);
    }

    /**
     * 删除goods对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsByIds(String ids)
    {
        return goodsMapper.deleteGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除goods信息
     * 
     * @param gid goodsID
     * @return 结果
     */
    @Override
    public int deleteGoodsById(Long gid)
    {
        return goodsMapper.deleteGoodsById(gid);
    }

    @Override
    public List < Goods > getData ( ) {
        return goodsMapper.getData();
    }

    @Override
    public Double getCtn ( ) {
        Map<String, Double> map = new HashMap();
        map.put("outNum",0.0);
        goodsMapper.getCtn ( map);
        return map.get("outNum");
    }
}
