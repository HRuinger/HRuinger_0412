package com.ruoyi.project.system.goods.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * goods对象 goods
 * 
 * @author HRuinger
 * @date 2021-04-12
 */
public class Goods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标识 */
    private Long gid;

    /** 名称 */
    @Excel(name = "名称")
    private String gname;

    /** 数量 */
    @Excel(name = "数量")
    private Long gxl;

    private BigDecimal zje;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal gprice;

    public BigDecimal getZje ( ) {
        return zje;
    }

    public void setZje ( BigDecimal zje ) {
        this.zje = zje;
    }

    public void setGid( Long gid)
    {
        this.gid = gid;
    }

    public Long getGid()
    {
        return gid;
    }
    public void setGname(String gname)
    {
        this.gname = gname;
    }

    public String getGname()
    {
        return gname;
    }
    public void setGxl(Long gxl)
    {
        this.gxl = gxl;
    }

    public Long getGxl()
    {
        return gxl;
    }
    public void setGprice(BigDecimal gprice)
    {
        this.gprice = gprice;
    }

    public BigDecimal getGprice()
    {
        return gprice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("gid", getGid())
            .append("gname", getGname())
            .append("gxl", getGxl())
            .append("gprice", getGprice())
            .toString();
    }
}
