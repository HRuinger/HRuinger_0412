package com.ruoyi.project.system.goods.service;

import java.util.List;
import com.ruoyi.project.system.goods.domain.Goods;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;

/**
 * goodsService接口
 * 
 * @author HRuinger
 * @date 2021-04-12
 */
public interface IGoodsService 
{
    /**
     * 查询goods
     * 
     * @param gid goodsID
     * @return goods
     */
    public Goods selectGoodsById(Long gid);

    /**
     * 查询goods列表
     * 
     * @param goods goods
     * @return goods集合
     */
    public List<Goods> selectGoodsList(Goods goods);

    /**
     * 新增goods
     * 
     * @param goods goods
     * @return 结果
     */
    public int insertGoods(Goods goods);

    /**
     * 修改goods
     * 
     * @param goods goods
     * @return 结果
     */
    public int updateGoods(Goods goods);

    /**
     * 批量删除goods
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsByIds(String ids);

    /**
     * 删除goods信息
     * 
     * @param gid goodsID
     * @return 结果
     */
    public int deleteGoodsById(Long gid);

    public List<Goods> getData();

    public Double getCtn();
}
